import { createRouter, createWebHistory } from "vue-router";
import Home from '../views/Home.vue'
import Projects from "../views/Projects.vue";
import Snake from "../views/Snake.vue";
import Hangman from "../views/Hangman.vue";
import QuizResults from "../views/QuizResults.vue";
import ToDoList from "../views/ToDoList.vue";
import MagicTheGathering from "../views/MagicTheGathering.vue";
import ProjectLinks from '../views/ProjectLinks.vue';

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,    
  },
  {
    path: "/projects",
    name: "Projects",
    component: Projects,
    children: [
      {
        path: '/projects',
        name: 'ProjectLinks',
        component: ProjectLinks,
      },
      {
        path: "snake",
        name: "Snake",
        component: Snake
      },
      {
        path: "hangman",
        name: "Hangman",
        component: Hangman
      },
      {
        path: "quiz",
        name: "Quiz",
        component: QuizResults
      },
      {
        path: "todolist",
        name: "ToDoList",
        component: ToDoList
      },
      {
        path: "magicthegathering",
        name: "MagicTheGathering",
        component: MagicTheGathering
      }
    ]
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
});

export default router;
